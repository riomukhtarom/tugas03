import java.util.*;
class geometryObjek extends MainMenu{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		SubMenuTwoDimensionalGeometry SubMenuTwoDimensionalGeometry = new SubMenuTwoDimensionalGeometry();
		SubMenuThreeDimensionalGeometry SubMenuThreeDimensionalGeometry = new SubMenuThreeDimensionalGeometry();
		MainMenu MainMenu = new MainMenu();
		
		int repeat=1;
		System.out.println("Program Menghitung Luas dan Keliling Bangun Datar serta Volume dan Luas Permukaan Bangun Ruang");
		do{
			MainMenu.display();
			try{
				int choise = input.nextInt();
				switch(choise){
					case 1:
						SubMenuTwoDimensionalGeometry.display();
						repeat=1;
						break;
					case 2:
						SubMenuThreeDimensionalGeometry.display();
						repeat=1;
						break;
					case 3:
						repeat = 0;
						break;
					default:
						System.out.println("Pilihan Salah");
						repeat = 1;
						break;
				}
			}catch(InputMismatchException e){
				System.out.println("Masukkan Bilangan Bulat Positif!!!");
				input.nextLine();
				repeat=1;
				continue;
			}
		}while(repeat==1);
		
	}
}
	
class MainMenu{
	void display(){
		System.out.println("Silahkan Pilih Bangun yang Akan dihitung (1-3)");
		System.out.println("1. Bangun Datar");
		System.out.println("2. Bangun Ruang");
		System.out.println("3. Keluar");
		System.out.print("==>");
	}
}
	
class SubMenuTwoDimensionalGeometry extends MainMenu{
	void display(){
		Scanner input = new Scanner(System.in);
		
		SubMenuAreaAndPerimeter SubMenuAreaAndPerimeter = new SubMenuAreaAndPerimeter();
		AreaofSquare AreaofSquare = new AreaofSquare();
		PerimeterofSquare PerimeterofSquare = new PerimeterofSquare();
		AreaofRectangle AreaofRectangle = new AreaofRectangle();
		PerimeterofRectangle PerimeterofRectangle = new PerimeterofRectangle();
		AreaofTriangle AreaofTriangle = new AreaofTriangle();
		PerimeterofTriangle PerimeterofTriangle = new PerimeterofTriangle();
		AreaofEquilateralTriangle AreaofEquilateralTriangle = new AreaofEquilateralTriangle();
		PerimeterofEquilateralTriangle PerimeterofEquilateralTriangle = new PerimeterofEquilateralTriangle();
		AreaofTrapezoid AreaofTrapezoid = new AreaofTrapezoid();
		PerimeterofTrapezoid PerimeterofTrapezoid = new PerimeterofTrapezoid();
		AreaofParallelogram AreaofParallelogram = new AreaofParallelogram();
		PerimeterofParallelogram PerimeterofParallelogram = new PerimeterofParallelogram();
		AreaofCircle AreaofCircle = new AreaofCircle();
		PerimeterofCircle PerimeterofCircle = new PerimeterofCircle();
		
		int repeat=1;
		do{
			System.out.println("Bangun Datar");
			System.out.println("1. Persegi");
			System.out.println("2. Persegi Panjang");
			System.out.println("3. Segitiga Sama Kaki");
			System.out.println("4. Segitiga Sama Sisi");
			System.out.println("5. Trapesium");
			System.out.println("6. Jajaran Genjang");
			System.out.println("7. Lingkaran");
			System.out.print("==>");

			try{
				int choice = input.nextInt();
				switch(choice){
					case 1:
						repeat=1;
						do{
							try{
								SubMenuAreaAndPerimeter.display();
								int choice1 = input.nextInt();
								switch(choice1){
									case 1:
										AreaofSquare.calculate();
										repeat=0;
										break;
									case 2:
										PerimeterofSquare.calculate();
										repeat=0;
										break;
									default:
										System.out.println("Pilihan Salah");
										break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 2:
						repeat=1;
						do{
							try{
								SubMenuAreaAndPerimeter.display();
								int choice2 = input.nextInt();
								switch(choice2){
									case 1:
										AreaofRectangle.calculate();
										repeat=0;
										break;
									case 2:
										PerimeterofRectangle.calculate();
										repeat=0;
										break;
									default:
										System.out.println("Pilihan Salah");
										break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 3:
						repeat=1;
						do{
							try{
								SubMenuAreaAndPerimeter.display();
								int choice3 = input.nextInt();
								switch(choice3){
									case 1:
										AreaofTriangle.calculate();
										repeat=0;
										break;
									case 2:
										PerimeterofTriangle.calculate();
										repeat=0;
										break;
									default:
										System.out.println("Pilihan Salah");
										break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 4:
						repeat=1;
						do{
							try{
								SubMenuAreaAndPerimeter.display();
								int choice4 = input.nextInt();
								switch(choice4){
									case 1:
										AreaofEquilateralTriangle.calculate();
										repeat=0;
										break;
									case 2:
										PerimeterofEquilateralTriangle.calculate();
										repeat=0;
										break;
									default:
										System.out.println("Pilihan Salah");
										break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 5:
						repeat=1;
						do{
							try{
								SubMenuAreaAndPerimeter.display();
								int choice5 = input.nextInt();
								switch(choice5){
									case 1:
										AreaofTrapezoid.calculate();
										repeat=0;
										break;
									case 2:
										PerimeterofTrapezoid.calculate();
										repeat=0;
										break;
									default:
										System.out.println("Pilihan Salah");
										break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 6:
						repeat=1;
						do{
							try{
								SubMenuAreaAndPerimeter.display();
								int choice6 = input.nextInt();
								switch(choice6){
									case 1:
										AreaofParallelogram.calculate();
										repeat=0;
										break;
									case 2:
										PerimeterofParallelogram.calculate();
										repeat=0;
										break;
									default:
										System.out.println("Pilihan Salah");
										break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 7:
						repeat=1;
						do{
							try{
								SubMenuAreaAndPerimeter.display();
								int choice7 = input.nextInt();
								switch(choice7){
									case 1:
										AreaofCircle.calculate();
										repeat=0;
										break;
									case 2:
										PerimeterofCircle.calculate();
										repeat=0;
										break;
									default:
										System.out.println("Pilihan Salah");
										break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					default:
							System.out.println("Pilihan Salah");
							break;
				}
				repeat=0;
			}catch(InputMismatchException e){
					System.out.println("Masukkan Bilangan Bulat Positif!!!");
					input.nextLine();
					repeat=1;
			}

		}while(repeat==1);
	}
}
	
class SubMenuThreeDimensionalGeometry extends MainMenu{
	void display(){
		Scanner input = new Scanner(System.in);
			
		SubMenuVolumeAndSurfaceArea SubMenuVolumeAndSurfaceArea = new SubMenuVolumeAndSurfaceArea();
		VolumeofCube VolumeofCube = new VolumeofCube();
		SurfaceAreaofCube SurfaceAreaofCube = new SurfaceAreaofCube();
		VolumeofBeam VolumeofBeam = new VolumeofBeam();
		SurfaceAreaofBeam SurfaceAreaofBeam = new SurfaceAreaofBeam();
		VolumeofPrism VolumeofPrism = new VolumeofPrism();
		SurfaceAreaofPrism SurfaceAreaofPrism = new SurfaceAreaofPrism();
		VolumeofTetrahedron VolumeofTetrahedron = new VolumeofTetrahedron();
		SurfaceAreaofTetrahedron SurfaceAreaofTetrahedron = new SurfaceAreaofTetrahedron();
		VolumeofPyramid VolumeofPyramid = new VolumeofPyramid();
		SurfaceAreaofPyramid SurfaceAreaofPyramid = new SurfaceAreaofPyramid();
		VolumeofCylinder VolumeofCylinder = new VolumeofCylinder();
		SurfaceAreaofCylinder SurfaceAreaofCylinder = new SurfaceAreaofCylinder();
		VolumeofCone VolumeofCone = new VolumeofCone();
		SurfaceAreaofCone SurfaceAreaofCone = new SurfaceAreaofCone();
		VolumeofSphere VolumeofSphere = new VolumeofSphere();
		SurfaceAreaofSphere SurfaceAreaofSphere = new SurfaceAreaofSphere();
	
		int repeat=1;
		do{
			System.out.println("Bangun Ruang");
			System.out.println("1. Kubus");
			System.out.println("2. Balok");
			System.out.println("3. Prisma Segitiga Samasisi");
			System.out.println("4. Limas Segitiga");
			System.out.println("5. Limas Segiempat");
			System.out.println("6. Tabung");
			System.out.println("7. Kerucut");
			System.out.println("8. Bola");
			System.out.print("==>");
			try{
				int choice = input.nextInt();
				switch(choice){
					case 1:
						repeat=1;
						do{
							try{
								SubMenuVolumeAndSurfaceArea.display();
								int choice1 = input.nextInt();
								switch(choice1){
									case 1:
										VolumeofCube.calculate();
										repeat=0;
										break;
									case 2:
										SurfaceAreaofCube.calculate();
										repeat=0;
										break;
									default:
										System.out.println("Pilihan Salah");
										break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 2:
						repeat=1;
						do{
							try{
								SubMenuVolumeAndSurfaceArea.display();
								int choice2 = input.nextInt();
								switch(choice2){
									case 1:
										VolumeofBeam.calculate();
										repeat=0;
										break;
									case 2:
										SurfaceAreaofBeam.calculate();
										repeat=0;
										break;
									default:
										System.out.println("Pilihan Salah");
										break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 3:
						repeat=1;
						do{
							try{
								SubMenuVolumeAndSurfaceArea.display();
								int choice3 = input.nextInt();
								switch(choice3){
									case 1:
										VolumeofPrism.calculate();
										repeat=0;
										break;
									case 2:
										SurfaceAreaofPrism.calculate();
										repeat=0;
										break;
									default:
										System.out.println("Pilihan Salah");
										break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 4:
						repeat=1;
						do{
							try{
								SubMenuVolumeAndSurfaceArea.display();
								int choice4 = input.nextInt();
								switch(choice4){
									case 1:
										VolumeofTetrahedron.calculate();
										repeat=0;
										break;
									case 2:
										SurfaceAreaofTetrahedron.calculate();
										repeat=0;
										break;
									default:
										System.out.println("Pilihan Salah");
										break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 5:
						repeat=1;
						do{
							try{    
								SubMenuVolumeAndSurfaceArea.display();
								int choice5 = input.nextInt();
								switch(choice5){
									case 1:
										VolumeofPyramid.calculate();
										repeat=0;
										break;
									case 2:
										SurfaceAreaofPyramid.calculate();
										repeat=0;
										break;
									default:
										System.out.println("Pilihan Salah");
										break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 6:
						repeat=1;
						do{
							try{
								SubMenuVolumeAndSurfaceArea.display();
								int choice6 = input.nextInt();
								switch(choice6){
									case 1:
										VolumeofCylinder.calculate();
										repeat=0;
										break;
									case 2:
										SurfaceAreaofCylinder.calculate();
										repeat=0;
										break;
									default:
										System.out.println("Pilihan Salah");
										break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 7:
						repeat=1;
						do{
							try{
								SubMenuVolumeAndSurfaceArea.display();
								int choice7 = input.nextInt();
								switch(choice7){
									case 1:
										VolumeofCone.calculate();
										repeat=0;
										break;
									case 2:
										SurfaceAreaofCone.calculate();
										repeat=0;
										break;
									default:
										System.out.println("Pilihan Salah");
										break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					case 8:
						repeat=1;
						do{
							try{
								SubMenuVolumeAndSurfaceArea.display();
								int choice8 = input.nextInt();
								switch(choice8){
									case 1:
										VolumeofSphere.calculate();
										repeat=0;
										break;
									case 2:
										SurfaceAreaofSphere.calculate();
										repeat=0;
										break;
									default:
										System.out.println("Pilihan Salah");
										break;
								}
							}catch(InputMismatchException e){
								System.out.println("Masukkan Bilangan Bulat Positif!!!");
								input.nextLine();
								repeat=1;
							}    
						}while(repeat==1);
						break;
					default:
						System.out.println("Pilihan Salah");
						break;
				}
			}catch(InputMismatchException e){
				System.out.println("Masukkan Bilangan Bulat Positif!!!");
				input.nextLine();
				repeat=1;
			}
			
		}while(repeat==1);
	}
}

class SubMenuAreaAndPerimeter extends SubMenuTwoDimensionalGeometry{
	void display(){
		System.out.println("Anda Ingin Menghitung Apa?");
		System.out.println("1. Luas");
		System.out.println("2. Keliling");
	}
}

class SubMenuVolumeAndSurfaceArea extends SubMenuThreeDimensionalGeometry{
	void display(){
		System.out.println("Anda Ingin Menghitung Apa?");
		System.out.println("1. Volume");
		System.out.println("2. Luas Permukaan");
	}
}

class AreaofSquare extends SubMenuTwoDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Sisi (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Luas Persegi = "+ Math.pow(length,2));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class PerimeterofSquare extends SubMenuTwoDimensionalGeometry{
	void calculate(){                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Sisi (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Keliling Persegi = "+((double) 4*length));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class AreaofRectangle extends SubMenuTwoDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Persegi Panjang (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Lebar Persegi Panjang (dalam cm) (Contoh: 34,5)");
				float width = input.nextFloat();
				System.out.println("Luas Persegi Panjang = "+((double) length*width));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class PerimeterofRectangle extends SubMenuTwoDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Persegi Panjang (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Lebar Persegi Panjang (dalam cm) (Contoh: 34,5)");
				float width = input.nextFloat();
				System.out.println("Keliling Persegi Panjang = "+((double) 2*(length+width)));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class AreaofTriangle extends SubMenuTwoDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Tinggi Segitiga (dalam cm) (Contoh:50,34)");
				float high = input.nextFloat();
				System.out.println("Masukkan Panjang Alas Segitiga (dalam cm) (Contoh: 34,5)");
				float length = input.nextFloat();
				System.out.println("Luas Segitiga = "+((double) 0.5*length*high));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class PerimeterofTriangle extends SubMenuTwoDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Sisi Segitiga yang sama panjang (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Panjang Alas Segitiga (dalam cm) (Contoh: 34,5)");
				float width = input.nextFloat();
				System.out.println("Keliling Segitiga = "+((double) 2*length+width));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class AreaofEquilateralTriangle extends SubMenuTwoDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Tinggi Segitiga (dalam cm) (Contoh:50,34)");
				float high = input.nextFloat();
				System.out.println("Masukkan Panjang Alas Segitiga (dalam cm) (Contoh: 34,5)");
				float length = input.nextFloat();
				System.out.println("Luas Segitiga = "+((double) 0.5*length*high));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class PerimeterofEquilateralTriangle extends SubMenuTwoDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Sisi Segitiga yang sama panjang (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Keliling Segitiga = "+((double) 3*length));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class AreaofTrapezoid extends SubMenuTwoDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Tinggi Trapesium (dalam cm) (Contoh:50,34)");
				float high = input.nextFloat();
				System.out.println("Masukkan Panjang Sisi Atas Trapesium (dalam cm) (Contoh: 34,5)");
				float length_a = input.nextFloat();
				System.out.println("Masukkan Panjang Sisi Bawah Trapesium (dalam cm) (Contoh: 34,5)");
				float length_b = input.nextFloat();
				System.out.println("Luas Trapesium = "+((double) 0.5*(length_a+length_b)*high));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class PerimeterofTrapezoid extends SubMenuTwoDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Sisi-sisinya (dalam cm) (Contoh:50,34)");
				System.out.println("Sisi 1");
				float length_a = input.nextFloat();
				System.out.println("Sisi 2");
				float length_b = input.nextFloat();
				System.out.println("Sisi 3");
				float length_c = input.nextFloat();
				System.out.println("Sisi 4");
				float length_d = input.nextFloat();
				System.out.println("Keliling Trapesium = "+((double) length_a+length_b+length_c+length_d));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class AreaofParallelogram extends SubMenuTwoDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Tinggi Jajaran Genjang (dalam cm) (Contoh:50,34)");
				float high = input.nextFloat();
				System.out.println("Masukkan Panjang Alas Jajaran Genjang (dalam cm) (Contoh: 34,5)");
				float length = input.nextFloat();
				System.out.println("Luas Jajaran Genjang = "+((double) 0.5*length*high));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class PerimeterofParallelogram extends SubMenuTwoDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Alas Jajaran Genjang (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Panjang Sisi Miring (dalam cm) (Contoh: 34,5)");
				float width = input.nextFloat();
				System.out.println("Keliling Jajaran Genjang = "+((double) 2*(length+width)));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class AreaofCircle extends SubMenuTwoDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari Lingkaran (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Luas Lingkaran = "+((double) 3.14*radius*radius));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class PerimeterofCircle extends SubMenuTwoDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari Lingkaran (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Keliling Segitiga = "+((double) 2*3.14*radius));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class VolumeofCube extends SubMenuThreeDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang sisi kubus (dalam cm) (Contoh:50,34)");
				float side = input.nextFloat();
				System.out.println("Volume Kubus = "+((double) side*side*side));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class SurfaceAreaofCube extends SubMenuThreeDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang sisi kubus (dalam cm) (Contoh:50,34)");
				float side = input.nextFloat();
				System.out.println("Luas Permukaan Kubus = "+((double) 6*side*side));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class VolumeofBeam extends SubMenuThreeDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Balok (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Lebar Balok (dalam cm) (Contoh:50,34)");
				float width = input.nextFloat();
				System.out.println("Masukkan Tinggi Balok (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Volume Balok = "+((double) length*width*height));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class SurfaceAreaofBeam extends SubMenuThreeDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Balok (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Lebar Balok (dalam cm) (Contoh:50,34)");
				float width = input.nextFloat();
				System.out.println("Masukkan Tinggi Balok (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Luas Permukaan Balok = "+((double) 2*((length*width)+(width*height)+(length*height))));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class VolumeofPrism extends SubMenuThreeDimensionalGeometry{
	void calculate(){ 
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Alas Segitiga (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Tinggi Segitiga (dalam cm) (Contoh:50,34)");
				float heighttriangle = input.nextFloat();
				System.out.println("Masukkan Tinggi Prisma (dalam cm) (Contoh:50,34)");
				float heightprism = input.nextFloat();
				System.out.println("Volume Prisma = "+((double) 0.5*length*heighttriangle*heightprism));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class SurfaceAreaofPrism extends SubMenuThreeDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang Alas Segitiga (dalam cm) (Contoh:50,34)");
				float length = input.nextFloat();
				System.out.println("Masukkan Tinggi Segitiga (dalam cm) (Contoh:50,34)");
				float heighttriangle = input.nextFloat();
				System.out.println("Masukkan Tinggi Prisma (dalam cm) (Contoh:50,34)");
				float heightprism = input.nextFloat();
				System.out.println("Luas Permukaan Prisma = "+((double) (2*(0.5*length*heighttriangle*heightprism)+3*(length*heightprism))));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class VolumeofTetrahedron extends SubMenuThreeDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang sisi Limas (dalam cm) (Contoh:50,34)");
				float side = input.nextFloat();
				System.out.println("Volume Limas Segitiga = "+((double) 1/12*Math.sqrt(2*Math.pow(side,3))));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class SurfaceAreaofTetrahedron extends SubMenuThreeDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang sisi Limas (dalam cm) (Contoh:50,34)");
				float side = input.nextFloat();
				System.out.println("Luas Permukaan Limas Segitiga = "+((double) Math.sqrt(3*Math.pow(side,2))));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class VolumeofPyramid extends SubMenuThreeDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang sisi Persegi (dalam cm) (Contoh:50,34)");
				float side = input.nextFloat();
				System.out.println("Masukkan Tinggi Limas (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Volume Limas Segiempat = "+((double) 1/3*Math.pow(side,2)*height));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class SurfaceAreaofPyramid extends SubMenuThreeDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Panjang sisi Persegi (dalam cm) (Contoh:50,34)");
				float side = input.nextFloat();
				System.out.println("Masukkan Tinggi Limas (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Luas Permukaan Limas Segitiga = "+((double) side*(side+Math.sqrt(Math.pow(side,2)+4*Math.pow(height,2)))));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class VolumeofCylinder extends SubMenuThreeDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Masukkan Tinggi Tabung (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Volume Tabung = "+((double) 3.14*Math.pow(radius,2)*height));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class SurfaceAreaofCylinder extends SubMenuThreeDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Masukkan Tinggi Tabung (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Luas Permukaan Tabung = "+((double) 2*3.14*radius*(height+radius)));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class VolumeofCone  extends SubMenuThreeDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Masukkan Tinggi Kerucut (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Volume Kerucut = "+((double) 1/3*3.14*Math.pow(radius,2)*height));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class SurfaceAreaofCone extends SubMenuThreeDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Masukkan Tinggi Kerucut (dalam cm) (Contoh:50,34)");
				float height = input.nextFloat();
				System.out.println("Luas Permukaan Kerucut = "+((double) 3.14*radius*(Math.sqrt(Math.pow(height,2)+Math.pow(radius,2))+radius)));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class VolumeofSphere extends SubMenuThreeDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Volume Bola = "+((double) 4/3*3.14*Math.pow(radius,3)));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}

class SurfaceAreaofSphere extends SubMenuThreeDimensionalGeometry{
	void calculate(){
		Scanner input = new Scanner(System.in);
		int repeat=0;
		do{
			try{
				System.out.println("Masukkan Jari-jari (dalam cm) (Contoh:50,34)");
				float radius = input.nextFloat();
				System.out.println("Luas Permukaan Bola = "+((double) 4*3.14*Math.pow(radius,2)));
				repeat=0;
			}catch(InputMismatchException e){
				System.out.println("Masukkan Angka!!!");
				input.nextLine();
				repeat=1;
			}catch(ArithmeticException e){
				System.out.println("Inputan Salah!!!");
				input.nextLine();
				repeat=1;
			}
		}while(repeat==1);
	}
}