import java.util.*;
public class matriksObjek{
    public static void main (String[] args){
        matrixequals matrixequals = new matrixequals();
        matrixdifference matrixdifference = new matrixdifference();
        matrixproduct matrixproduct = new matrixproduct();
        
        int repeat = 0;
        System.out.println("Program Operasi Matriks");
        do{
            Scanner input = new Scanner(System.in);
            System.out.println("Pilih Operasi Matriks yang diinginkan (1-4): ");
            System.out.println("1. Penjumlahan Matriks");
            System.out.println("2. Pengurangan Matriks");
            System.out.println("3. Perkalian Matriks");
            System.out.println("4. Keluar");
            System.out.print("==>");
            try{
                int choice = input.nextInt();
                switch(choice){
                    case 1:
                        matrixequals.display();
                        repeat=1;
                        break;
                    case 2:
                        matrixdifference.display();
                        repeat=1;
                        break;
                    case 3:
                        matrixproduct.display();
                        break;
                    case 4:
                        repeat=0;
                        break;
                    default:
                        System.out.println("Masukkan Angka Antara 1-4");
                        repeat=1;
                        break;
                }
            }catch(InputMismatchException e){
                System.out.println("Masukkan Angka Antara 1-4");
                input.nextLine();
                repeat=1;
            }
        }while(repeat==1);
    }
}
    class matrixequals{
        void display(){
            Scanner input = new Scanner(System.in);
            int repeat=1;
            do{
                try{
                    System.out.println("Matriks A");
                    System.out.print("Jumlah Baris : ");
                    int row_a = input.nextInt();
                    System.out.print("Jumlah Kolom : ");
                    int column_a = input.nextInt();
                    int[][] matriks1 = new int[row_a][column_a];

                    if(row_a ==0 || column_a ==0){
                        System.out.println("Masukkan Bilangan Bulat Positif!!!");
                        repeat=1;
                        continue;
                    }

                    for(int indeks1=0; indeks1<row_a; indeks1++){
                        for(int indeks2=0; indeks2<column_a; indeks2++){
                            System.out.print("Baris ke-"+(indeks1+1)+" Kolom ke-"+(indeks2+1)+" : ");
                            matriks1[indeks1][indeks2] = input.nextInt();
                        }
                    }
                    System.out.println("Matriks B");
                    System.out.print("Jumlah Baris : ");
                    int row_b = input.nextInt();
                    System.out.print("Jumlah Kolom : ");
                    int column_b = input.nextInt();
                    int[][] matriks2 = new int[row_b][column_b];

                    if(row_b ==0 || column_b ==0){
                        System.out.println("Masukkan Bilangan Bulat Positif!!!");
                        repeat=1;
                        continue;
                    }

                    for(int indeks1=0; indeks1<row_b; indeks1++){
                        for(int indeks2=0; indeks2<column_b; indeks2++){
                            System.out.print("Baris ke-"+(indeks1+1)+" Kolom ke-"+(indeks2+1)+" : ");
                            matriks2[indeks1][indeks2] = input.nextInt();
                        }
                    }

                    System.out.println("\nMatriks A");
                    for(int indeks1=0; indeks1<row_a; indeks1++){
                        for(int indeks2=0; indeks2<column_a; indeks2++){
                            System.out.printf("%4d",matriks1[indeks1][indeks2]);
                        }
                        System.out.println();
                    }
                    System.out.println("\nMatriks B");
                    for(int indeks1=0; indeks1<row_b; indeks1++){
                        for(int indeks2=0; indeks2<column_b; indeks2++){
                            System.out.printf("%4d",matriks2[indeks1][indeks2]);
                        }
                        System.out.println();
                    }

                    int[][] matriks3 = new int[row_a][column_b];

                    if(row_a==row_b && column_a==column_b){
                        for(int indeks1=0; indeks1<row_a; indeks1++){
                            for(int indeks2=0; indeks2<column_a; indeks2++){
                                matriks3[indeks1][indeks2] = matriks1[indeks1][indeks2]+matriks2[indeks1][indeks2];
                            }
                        }
                    }else{
                        System.out.println("\nTidak dapat dijumlahkan\n");
                        repeat=0;
                        continue;
                    }
                    System.out.println();
                    System.out.println("Hasil Penjumlahannya : ");
                    for(int indeks1=0; indeks1<row_a; indeks1++){
                        for(int indeks2=0; indeks2<column_b; indeks2++){
                            System.out.printf("%4d",matriks3[indeks1][indeks2]);
                        }
                        System.out.println();
                    }
                    repeat=0;
                    System.out.println();
                }catch(InputMismatchException e){
                    System.out.println("Masukkan Bilangan Bulat Positif!!!");
                    input.nextLine();
                    repeat=1;
                }catch(NegativeArraySizeException e){
                    System.out.println("Masukkan Bilangan Bulat Positif!!!");
                    input.nextLine();
                    repeat=1;
                }
            }while(repeat==1);
        }
    }
	
    class matrixdifference{
        void display(){
            Scanner input = new Scanner(System.in);
            int repeat = 1;
            do{
                try{
                    System.out.println("Matriks A");
                    System.out.print("Jumlah Baris : ");
                    int row_a = input.nextInt();
                    System.out.print("Jumlah Kolom : ");
                    int column_a = input.nextInt();
                    int[][] matriks1 = new int[row_a][column_a];

                                    if(row_a ==0 || column_a ==0){
                        System.out.println("Masukkan Bilangan Bulat Positif!!!");
                        repeat=1;
                        continue;
                    }

                    for(int indeks1=0; indeks1<row_a; indeks1++){
                        for(int indeks2=0; indeks2<column_a; indeks2++){
                            System.out.print("Baris ke-"+(indeks1+1)+" Kolom ke-"+(indeks2+1)+" : ");
                            matriks1[indeks1][indeks2] = input.nextInt();
                        }
                    }
                    System.out.println("Matriks B");
                    System.out.print("Jumlah Baris : ");
                    int row_b = input.nextInt();
                    System.out.print("Jumlah Kolom : ");
                    int column_b = input.nextInt();
                    int[][] matriks2 = new int[row_b][column_b];

                                    if(row_b ==0 || column_b ==0){
                        System.out.println("Masukkan Bilangan Bulat Positif!!!");
                        repeat=1;
                        continue;
                    }

                    for(int indeks1=0; indeks1<row_b; indeks1++){
                        for(int indeks2=0; indeks2<column_b; indeks2++){
                            System.out.print("Baris ke-"+(indeks1+1)+" Kolom ke-"+(indeks2+1)+" : ");
                            matriks2[indeks1][indeks2] = input.nextInt();
                        }
                    }

                    System.out.println("\nMatriks A");
                    for(int indeks1=0; indeks1<row_a; indeks1++){
                        for(int indeks2=0; indeks2<column_a; indeks2++){
                            System.out.printf("%4d",matriks1[indeks1][indeks2]);
                        }
                        System.out.println();
                    }

                    System.out.println("\nMatriks B");
                    for(int indeks1=0; indeks1<row_b; indeks1++){
                        for(int indeks2=0; indeks2<column_b; indeks2++){
                            System.out.printf("%4d",matriks2[indeks1][indeks2]);
                        }
                        System.out.println();
                    }

                    int[][] matriks3 = new int[row_a][column_b];

                    if(row_a==row_b && column_a==column_b){
                        for(int indeks1=0; indeks1<row_a; indeks1++){
                            for(int indeks2=0; indeks2<column_b; indeks2++){
                                matriks3[indeks1][indeks2] = matriks1[indeks1][indeks2]-matriks2[indeks1][indeks2];
                            }
                        }
                    }else{
                        System.out.println("\nTidak dapat dikurangkan\n");
                        repeat=0;
                        continue;
                    }
                    System.out.println();
                    System.out.println("Hasil Pengurangannya : ");
                    for(int indeks1=0; indeks1<row_a; indeks1++){
                        for(int indeks2=0; indeks2<column_b; indeks2++){
                            System.out.printf("%4d",matriks3[indeks1][indeks2]);
                        }
                        System.out.println();
                    }
                    System.out.println();
                    repeat=0;
                }catch(InputMismatchException e){
                    System.out.println("Masukkan Bilangan Bulat Positif!!!");
                    input.nextLine();
                    repeat=1;
                }catch(NegativeArraySizeException e){
                    System.out.println("Masukkan Bilangan Bulat Positif!!!");
                    input.nextLine();
                    repeat=1;
                }
            }while(repeat==1);
        }
    }
	
    class matrixproduct{
        void display(){
            Scanner input = new Scanner(System.in);
            int repeat = 1;
            do{
                try{
                    System.out.println("Matriks A");
                    System.out.print("Jumlah Baris : ");
                    int row_a = input.nextInt();
                    System.out.print("Jumlah Kolom : ");
                    int column_a = input.nextInt();
                    int[][] matriks1 = new int[row_a][column_a];

                                    if(row_a ==0 || column_a ==0){
                        System.out.println("Masukkan Bilangan Bulat Positif!!!");
                        repeat=1;
                        continue;
                    }

                    for(int indeks1=0; indeks1<row_a; indeks1++){
                        for(int indeks2=0; indeks2<column_a; indeks2++){
                            System.out.print("Baris ke-"+(indeks1+1)+" Kolom ke-"+(indeks2+1)+" : ");
                            matriks1[indeks1][indeks2] = input.nextInt();
                        }
                    }
                    System.out.println("Matriks B");
                    System.out.print("Jumlah Baris : ");
                    int row_b = input.nextInt();
                    System.out.print("Jumlah Kolom : ");
                    int column_b = input.nextInt();
                    int[][] matriks2 = new int[row_b][column_b];

                        if(row_b ==0 || column_b ==0){
                        System.out.println("Masukkan Bilangan Bulat Positif!!!");
                        repeat=1;
                        continue;
                    }

                    for(int indeks1=0; indeks1<row_b; indeks1++){
                        for(int indeks2=0; indeks2<column_b; indeks2++){
                            System.out.print("Baris ke-"+(indeks1+1)+" Kolom ke-"+(indeks2+1)+" : ");
                            matriks2[indeks1][indeks2] = input.nextInt();
                        }
                    }

                    System.out.println("\nMatriks A");
                    for(int indeks1=0; indeks1<row_a; indeks1++){
                        for(int indeks2=0; indeks2<column_a; indeks2++){
                            System.out.printf("%4d",matriks1[indeks1][indeks2]);
                        }
                        System.out.println();
                    }

                    System.out.println("\nMatriks B");
                    for(int indeks1=0; indeks1<row_b; indeks1++){
                        for(int indeks2=0; indeks2<column_b; indeks2++){
                            System.out.printf("%4d",matriks2[indeks1][indeks2]);
                        }
                        System.out.println();
                    }

                    int[][] matriks3 = new int[row_a][column_b];

                    if(column_a==row_b){
                        for(int indeks1=0; indeks1<row_a; indeks1++){
                            for(int indeks2=0; indeks2<column_b; indeks2++){
                                int submatriks=0;
                                for(int indeks3=0; indeks3<column_a; indeks3++){
                                    submatriks += matriks1[indeks1][indeks3]*matriks2[indeks3][indeks2];
                                }
                                matriks3[indeks1][indeks2] = submatriks;
                            }
                        }
                    }else{
                        System.out.println("\nTidak dapat dikalikan\n");
                        repeat=0;
                        continue;
                    }

                    System.out.println("\nHasil Perkaliannya");
                    for(int indeks1=0; indeks1<row_a; indeks1++){
                        for(int indeks2=0; indeks2<column_b; indeks2++){
                            System.out.printf("%4d",matriks3[indeks1][indeks2]);
                        }
                        System.out.println();
                    }
                    System.out.println();
                    repeat=0;
                }catch(InputMismatchException e){
                    System.out.println("Masukkan Bilangan Bulat Positif!!!");
                    input.nextLine();
                    repeat=1;
                }catch(NegativeArraySizeException e){
                    System.out.println("Masukkan Bilangan Bulat Positif!!!");
                    input.nextLine();
                    repeat=1;
                }
            }while(repeat==1);
        }
    }
