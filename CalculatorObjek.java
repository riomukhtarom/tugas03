import java.util.*;
class CalculatorObjek {
	public static void main (String[] args){
		Scanner input = new Scanner(System.in);
		CalculatorButton CalculatorButton = new CalculatorButton();
		byte repeat = 1;
		System.out.print("Masukkan Angka Pertama : ");
		double firstNumber = input.nextDouble();
		System.out.print("Masukkan Angka Kedua : ");
		double secondNumber = input.nextDouble();
		System.out.println("Silahkan PIlih ");
		System.out.println("1. Penjumlahan");
		System.out.println("2. Pengurangan");
		System.out.println("3. Perkalian");
		System.out.println("4. Pembagian");
		System.out.print("==> ");
		
		do{
			try{
				int choice = input.nextInt();
				switch(choice){
					case 1:
						System.out.print("Hasilnya = "+CalculatorButton.addition(firstNumber,secondNumber));
						repeat = 1;
						continue;
					case 2:
						System.out.print("Hasilnya = "+CalculatorButton.subtraction(firstNumber,secondNumber));
						repeat = 1;
						continue;
					case 3:
						System.out.print("Hasilnya = "+CalculatorButton.product(firstNumber,secondNumber));
						repeat = 1;
						continue;
					case 4:
						System.out.print("Hasilnya = "+CalculatorButton.division(firstNumber,secondNumber));
						repeat = 1;
						continue;
					default :
						System.out.print("Pilihan Salah");
						repeat = 1;
						continue;
				}
			}catch(InputMismatchException e){
				System.out.println("Anda Tidak Memasukkan Angka");
				input.nextInt();
				break;
			}
		}while(repeat==1);
	}
}
class CalculatorButton extends Calculator{
	double firstNumber, secondNumber, equals;

	double addition(double firstNumber, double secondNumber){
		equals = firstNumber + secondNumber;
		return equals;
	}
	double subtraction(double firstNumber, double secondNumber){
		equals = firstNumber - secondNumber;
		return equals;
	}
	double product(double firstNumber, double secondNumber){
		equals = firstNumber * secondNumber;
		return equals;
	}
	double division(double firstNumber, double secondNumber){
		equals = firstNumber / secondNumber;
		return equals;
	}
}